package com.owner.kitpvp;

import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.regions.Region;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.owner.kitpvp.config.Config;
import com.owner.kitpvp.config.YamlConfig;

public class Main
        extends JavaPlugin
{

    private WorldEditPlugin we;
    private Region arenaRegion;
    private Region spawnRegion;
    private Config config;
    private Map<Player, PlayerClass> playersClasses;
    private Map<Player, Integer> playersLevels;

    @Override
    public void onEnable()
    {
        PluginManager pm = Bukkit.getServer().getPluginManager();

        this.we = ((WorldEditPlugin) pm.getPlugin("WorldEdit"));
        log("Loaded WorldEdit");

        pm.registerEvents(new PlayerListener(this), this);

        this.playersClasses = new HashMap<Player, PlayerClass>();
        this.playersLevels = new HashMap<Player, Integer>();
        
        this.config = new YamlConfig("plugins/KitPvP/config.yml", "plugins/KitPvP/classes.yml");
        this.config.loadSettings();
        this.config.loadClasses();
    }
    
    @Override
    public void onDisable()
    {
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        if ((command.getName().equalsIgnoreCase("kpvp")) && ((sender instanceof Player)))
        {
            if (args.length < 1)
            {
                return false;
            }
            Player player = (Player) sender;
            if (args[0].equalsIgnoreCase("create"))
            {
                if (args.length < 2)
                {
                    return false;
                }
                LocalSession session = this.we.getSession(player);
                if (args[1].equalsIgnoreCase("arena"))
                {
                    try
                    {
                        Region selection = session.getSelection(session.getSelectionWorld());
                        player.chat("Created arena selection");
                        this.arenaRegion = selection;
                    }
                    catch (IncompleteRegionException e)
                    {
                        player.chat("Incomplete region!");
                    }

                }
                else if (args[1].equalsIgnoreCase("spawn"))
                {
                    try
                    {
                        Region selection = session.getSelection(session.getSelectionWorld());
                        player.chat("Created spawn selection");
                        this.spawnRegion = selection;
                    }
                    catch (IncompleteRegionException e)
                    {
                        player.chat("Incomplete region!");
                    }
                }
            }
            return true;
        }
        return false;
    }

    public Region getArenaRegion()
    {
        return this.arenaRegion;
    }

    public Region getSpawnRegion()
    {
        return this.spawnRegion;
    }

    public WorldEditPlugin getWorldEdit()
    {
        return this.we;
    }
    
    public Config getPluginConfig()
    {
        return this.config;
    }
    
    public PlayerClass getPlayerClass(Player player)
    {
        return this.playersClasses.get(player);
    }
    
    public Main setPlayerClass(Player player, PlayerClass cls)
    {
        this.playersClasses.put(player, cls);
        return this;
    }
    
    public Integer getPlayerLevel(Player player)
    {
        return this.playersLevels.get(player);
    }
    
    public Main setPlayerLevel(Player player, int level)
    {
        this.playersLevels.put(player, level);
        return this;
    }

    public Main log(String log)
    {
        getLogger().log(Level.INFO, log);
        return this;
    }
}
