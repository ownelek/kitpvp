/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.owner.kitpvp.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.owner.kitpvp.PlayerClass;
import java.util.List;
import java.util.logging.Level;
import org.bukkit.enchantments.Enchantment;

/**
 *
 * @author Owner
 */
public class YamlConfig implements Config
{
    private final YamlConfiguration config;
    private final Set<PlayerClass> classes;
    private final String configFile;
    private final String classesFile;
    
    public YamlConfig(String configFile, String classesFile)
    {
        this.configFile = configFile;
        this.classesFile = classesFile;
        this.config = new YamlConfiguration();
        this.classes = new LinkedHashSet<PlayerClass>();
    }
    
    @Override
    public void loadSettings() {
        try
        {
            this.config.load(this.configFile);
        }
        catch (FileNotFoundException | InvalidConfigurationException e)
        {
            this.config.set("kpvp.rewards.kill", 5);
            this.config.set("kpvp.rewards.doublekill", 20);
            this.config.set("kpvp.rewards.addForNext", 10);
            this.config.set("kpvp.multikill.time", 15);
            try
            {
                this.config.save(this.configFile);
            }
            catch (IOException e2)
            {
            }
        }
        catch (IOException e)
        {
        }

        this.config.addDefault("kpvp.rewards.kill", 5);
        this.config.addDefault("kpvp.rewards.doublekill", 20);
        this.config.addDefault("kpvp.rewards.triplekill", 50);
        this.config.addDefault("kpvp.multikill.time", 15);
    }
    
    @Override
    public int getInt(String key)
    {
        return this.config.getInt(key);
    }
    
    @Override
    public String getString(String key)
    {
        return this.config.getString(key);
    }
    
    @Override
    public void loadClasses()
    {
        YamlConfiguration file = new YamlConfiguration();
        try
        {
            file.load(this.classesFile);
        }
        catch (FileNotFoundException e)
        {
            Bukkit.getLogger().severe("Classes file not found!");
            return;
        }
        catch(InvalidConfigurationException e)
        {
            Bukkit.getLogger().severe("Invalid configuration of classes file");
            e.printStackTrace();
            return;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return;
        }
        
        Set<String> set = file.getKeys(false);
        for (String key : set)
        {
            //Bukkit.getLogger().log(Level.INFO, key.toString());
            String name = (String) file.get(key + ".name");
            
            ItemStack classItem = new ItemStack(Material.getMaterial(file.get(key + ".item").toString()));
            
            MemorySection levelsSection = (MemorySection) file.get(key + ".levels");
            Set<String> levels = levelsSection.getKeys(false);
            List<Integer> levels_cost = new LinkedList<Integer>();
            List<List<ItemStack>> levels_items = new LinkedList<List<ItemStack>>();
            
            for(String level : levels)
            {
                List<Map<String, Object>> items = (List<Map<String, Object>>) levelsSection.get(level + ".items");
                List<ItemStack> isList = new LinkedList<ItemStack>();
                for(Map<String, Object> item : items)
                {
                    if(Material.getMaterial(item.get("type").toString()) == null)
                    {
                        Bukkit.getLogger().severe(String.format("Unknown material %s", item.get("type").toString()));
                        continue;
                    }
                    
                    ItemStack is = new ItemStack(Material.getMaterial(item.get("type").toString()), (int) item.get("amount"));
                    ItemMeta meta = is.getItemMeta();
                    meta.setDisplayName(item.get("name").toString());
                    is.setItemMeta(meta);
                    if(item.get("enchants") != null)
                    {
                        for(String enchant : (List<String>) item.get("enchants"))
                        {
                            String[] splitted = enchant.split(":");
                            is.addEnchantment(Enchantment.getByName(splitted[0]), Integer.valueOf(splitted[1]));
                        }
                    }
                    isList.add(is);
                }
                
                levels_cost.add((int) levelsSection.get(level + ".cost"));
                levels_items.add(isList);
            }
            
            this.classes.add(new PlayerClass(name, classItem, levels_cost, levels_items));
        }
        //classes.put(key, new PlayerClass());
    }
    
    @Override
    public Set<PlayerClass> getClasses()
    {
        return this.classes;
    }
}
