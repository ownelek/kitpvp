/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.owner.kitpvp.config;

import java.util.Set;

import com.owner.kitpvp.PlayerClass;

/**
 *
 * @author Owner
 */
public interface Config
{
    public int getInt(String key);
    public String getString(String key);
    public Set<PlayerClass> getClasses();
    public void loadSettings();
    public void loadClasses();
}
