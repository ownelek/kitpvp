package com.owner.kitpvp;

import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.regions.Region;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Player.Spigot;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class PlayerListener
        implements Listener
{
    private Main plugin;
    private HashMap<Player, HashMap<String, Object>> lastKill;
    private HashMap<Integer, String> latinToRoman;

    public PlayerListener(Main plugin)
    {
        this.plugin = plugin;
        this.lastKill = new HashMap();
        this.latinToRoman = new HashMap();
        this.latinToRoman.put(1, "I");
        this.latinToRoman.put(2, "II");
        this.latinToRoman.put(3, "III");
        this.latinToRoman.put(4, "IV");
        this.latinToRoman.put(5, "V");
        this.latinToRoman.put(6, "VI");
        this.latinToRoman.put(7, "VII");
    }
    
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event)
    {
        if(event.getRawSlot() == event.getSlot() &&
            event.getClickedInventory() != null && 
            event.getClickedInventory().getTitle().equals("Sklep Klas") &&
            event.getCurrentItem() != null &&
            event.getWhoClicked() instanceof Player)
        {
            ItemStack item = event.getCurrentItem();
            PlayerClass playerClass = null;
            Bukkit.getLogger().info("hi");
            for(PlayerClass pClass : plugin.getPluginConfig().getClasses())
            {
                Bukkit.getLogger().info(pClass.getItem().toString());
                if(pClass.getItem().getType() == item.getType())
                    playerClass = pClass;
            }
            
            if(playerClass == null)
                return;
            
            Player player = (Player) event.getWhoClicked();
            Bukkit.getLogger().info("HI");
            if(plugin.getPlayerLevel(player) != null && plugin.getPlayerLevel(player) >= playerClass.getMaxLevel())
            {
                player.sendMessage("Osiagnales maksymalny poziom tej klasy!");
            }
            else
            {
                int poziom;
                try
                {
                    poziom = plugin.getPlayerLevel(player) + 1;
                }
                catch(NullPointerException e)
                {
                    poziom = 0;
                }
                
                plugin.setPlayerClass(player, playerClass);
                plugin.setPlayerLevel(player, poziom);
                for(ItemStack is : playerClass.getItems(poziom))
                {
                    player.getInventory().addItem(is);
                }
                //player.getInventory().addItem((ItemStack[])playerClass.getItems(poziom).toArray());
                player.sendMessage(String.format(
                        "Kupiles poziom %d klasy %s za %d monet!",
                        poziom, playerClass.getName(), playerClass.getCost(poziom)
                ));
            }
            player.closeInventory();
            event.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event)
    {
        if(event.getItem().getType() == Material.STICK)
        {
            event.setCancelled(true);
            Inventory inv = Bukkit.createInventory(null, 54, "Sklep Klas");
            for(PlayerClass pClass : plugin.getPluginConfig().getClasses())
            {
                ItemStack it = new ItemStack(pClass.getItem());
                ItemMeta meta = it.getItemMeta();
                LinkedList<String> lore = new LinkedList();
                meta.setDisplayName(String.format("§6Klasa:§9 %s", pClass.getName()));
                Integer poziom = plugin.getPlayerLevel(event.getPlayer());
                if(poziom == null)
                {
                    poziom = 0;
                }
                else
                {
                    poziom += 1;
                }
                
                if(poziom >= pClass.getMaxLevel())
                {
                    lore.add("§cOsiagnales maksymalny poziom!");
                }
                else
                {
                    lore.add(String.format("§6Poziom:§9 %d", poziom));
                    lore.add("§6Przedmioty: ");
                    for(ItemStack is : pClass.getItems(poziom))
                    {
                        StringBuilder przedmiot = new StringBuilder();
                        przedmiot.append("§9").append(is.getItemMeta().getDisplayName());
                        if(is.getItemMeta().hasEnchants())
                        {
                            przedmiot.append("§6 (");
                            for(Entry<Enchantment, Integer> ench : is.getEnchantments().entrySet())
                            {
                                przedmiot.append(ench.getKey().getName())
                                        .append(" ")
                                        .append(this.latinToRoman.get(ench.getValue()))
                                        .append(",");
                            }
                            przedmiot.deleteCharAt(przedmiot.lastIndexOf(","));
                            przedmiot.append(")");
                        }
                        lore.add(przedmiot.toString());
                    }
                
                    lore.add(String.format("§6Cena:§9 %d monet", pClass.getCost(poziom)));
                    lore.add("§aKliknij aby kupic!");
                }
                meta.setLore(lore);
                it.setItemMeta(meta);
                inv.addItem(it);
            }
            event.getPlayer().openInventory(inv);
        }
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event)
    {
        Location entLocation = event.getEntity().getLocation();
        if ((this.plugin.getSpawnRegion() != null) && (this.plugin.getSpawnRegion().contains(new Vector(entLocation.getX(), entLocation.getY(), entLocation.getZ()))))
        {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event)
    {
        Player player = event.getEntity();
        Location loc = player.getLocation();
        if ((this.plugin.getArenaRegion() != null) && (this.plugin.getArenaRegion().contains(new Vector(loc.getX(), loc.getY(), loc.getZ()))))
        {
            Player killer = player.getKiller();
            if (killer == null)
            {
                event.setDeathMessage(player.getDisplayName() + " popełnił samobójstwo!");
            }
            else
            {
                event.setDeathMessage(killer.getDisplayName() + " zabił gracza " + player.getDisplayName());
                HashMap<String, Object> killInfo = (HashMap) this.lastKill.get(killer);
                if (killInfo == null)
                {
                    killInfo = new HashMap();
                    killInfo.put("timestamp", 0);
                    killInfo.put("multiKill", 0);
                    this.lastKill.put(killer, killInfo);
                }

                if (System.currentTimeMillis() - ((Long) killInfo.get("timestamp")) < 10000L)
                {
                    int multiKill = ((Integer) killInfo.get("multiKill")) + 1;
                    killInfo.put("multiKill", multiKill);
                    if (multiKill == 2)
                    {
                        this.plugin.getServer().broadcastMessage("Double kill!");
                    }
                    else if (multiKill == 3)
                    {
                        this.plugin.getServer().broadcastMessage("Triple kill!");
                    }
                    else if (multiKill == 4)
                    {
                        this.plugin.getServer().broadcastMessage("Quadra kill!");
                    }
                }
                else
                {
                    killInfo.put("multiKill", 1);
                }
            }

            Vector minSpawnPoint = this.plugin.getSpawnRegion().getMinimumPoint();
            Vector maxSpawnPoint = this.plugin.getSpawnRegion().getMaximumPoint();

            Double randX = minSpawnPoint.getX() + (Math.random() * maxSpawnPoint.getX() - minSpawnPoint.getX());
            Double randZ = minSpawnPoint.getZ() + (Math.random() * maxSpawnPoint.getZ() - minSpawnPoint.getZ());
            Double highY = player.getWorld().getHighestBlockYAt(randX.intValue(), randZ.intValue()) + 2.0D;

            Location teleportLocation = new Location(player.getWorld(), randX, highY, randZ);

            player.spigot().respawn();
            player.teleport(teleportLocation);
        }
    }
}
