package com.owner.kitpvp;

import java.util.List;
import org.bukkit.inventory.ItemStack;

public class PlayerClass
{
    private final String name;
    private final ItemStack classItem;
    private final List<Integer> levels_cost;
    private final List<List<ItemStack>> levels_items;

    public PlayerClass(String name, ItemStack classItem, List<Integer> levels_cost, List<List<ItemStack>> levels_items)
    {
        this.name = name;
        this.classItem = classItem;
        this.levels_cost = levels_cost;
        this.levels_items = levels_items;
    }

    public String getName()
    {
        return this.name;
    }
    
    public ItemStack getItem() {
        return this.classItem;
    }
    
    public int getCost(int level)
    {
        return this.levels_cost.get(level);
    }

    public List<ItemStack> getItems(int level)
    {
        return this.levels_items.get(level);
    }
    
    public int getMaxLevel()
    {
        return levels_cost.size();
    }
}

